﻿namespace DiPhoneCon;

/// <summary>
/// пользовательское взаимодействие уровня конференции и устройства
/// </summary>
public class UserInterface
{
    /// <summary>
    /// Вывод меню пользователю и получение от него резальтата
    /// </summary>
    /// <param name="items">множество элементов, из которых нужно выбрать</param>
    /// <param name="question">Вопрос для выбора</param>
    /// <param name="backTitle">название для команды возврата на шаг назад</param>
    /// <param name="itemNaming">функция именования пунта меню</param>
    /// <typeparam name="T">тип элементов, из которых пользователю нужно выбрать</typeparam>
    /// <returns>выбранный элемент и команда</returns>
    public UiResult<T> ShowMenu<T>(T[] items, string question, string backTitle, Func<T, string> itemNaming)
    {
        for (int i = 1; i <= items.Length; i++)
        {
            Console.WriteLine($"[{i}] - {itemNaming.Invoke(items[i-1])}");
        }

        if (backTitle != null) Console.Write($"{question}? (Enter - уйти, 0 - {backTitle}) >");
        else Console.Write($"{question}? (Enter - уйти) >");
        string userAnswer = Console.ReadLine();
        if (string.IsNullOrWhiteSpace(userAnswer)) return new UiResult<T>() { Code = UiCode.Exit };
        if (userAnswer == "0")
            if (backTitle != null) return new UiResult<T>() { Code = UiCode.Back };
            else return new UiResult<T>() { Code = UiCode.Exit };
        if (int.TryParse(userAnswer, out int variantNumber) == false)
        {
            Console.WriteLine("Мы не поняли, какое устройство вы хотите попробовать.");
            return new UiResult<T>() { Code = UiCode.Unknown };
        }

        if (variantNumber <= 0 || variantNumber > items.Length)
        {
            Console.WriteLine("У нас такого нет :(");
            return new UiResult<T>() { Code = UiCode.Unknown };
        }

        Console.WriteLine($"Вы выбрали {itemNaming.Invoke(items[variantNumber-1])}");
        Console.WriteLine("----------");
        return new UiResult<T>() { Code = UiCode.Variant, Payload = items[variantNumber-1] };
    }

    /// <summary>
    /// Взаимодействие с Модулем устройства
    /// </summary>
    /// <param name="currentDevice">устройство</param>
    /// <param name="currentModule">модйль устройства</param>
    /// <exception cref="NotImplementedException"></exception>
    public UiCode ModuleInteraction(BaseDevice currentDevice, IDeviceModule currentModule)
    {
        //За UI каждого модуля отвечает отдельный класс.
        //Выбрать класс и передать ему модуль для работы
        IUiModule ui = getUi(currentDevice, currentModule);
        return ui.RunModuleUi(currentDevice,currentModule);
    }

    /// <summary>
    /// По типизированному репозиторию выбираем Ui для взаимодействия с модулем
    /// </summary>
    /// <param name="currentDevice"></param>
    /// <param name="currentModule"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    private IUiModule getUi(BaseDevice currentDevice, IDeviceModule currentModule)
    {
        ModuleUiRepository modilesUi = new ModuleUiRepository();
        return modilesUi.GetModuleUi(currentModule);
    }
}