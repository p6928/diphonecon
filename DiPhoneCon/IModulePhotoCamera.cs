﻿namespace DiPhoneCon;

/// <summary>
/// Аппаратный модуль фотокамеры
/// </summary>
public interface IModulePhotoCamera
{
    /// <summary>
    /// Начать делать фотку. процесс фотографирования занимает некоторое время и выполняется фотокамерой без блокирования
    /// остальных процессов телефона
    /// </summary>
    /// <returns></returns>
    PhotoData MakePhoto();

    /// <summary>
    /// Состояние камеры, занята делом или готова к работе
    /// </summary>
    PhotoCameraState State { get; }

    /// <summary>
    /// Получить из внутренней памяти последнюю фотку
    /// </summary>
    /// <returns></returns>
    PhotoData GetLastPhoto();

    /// <summary>
    /// В буфере камеры есть фотка
    /// </summary>
    bool HasLastPhoto { get; }
}

/// <summary>
/// Состояние камеры
/// </summary>
public enum PhotoCameraState
{
    /// <summary>
    /// Готова к использованию, можно фотать или забрать последнюю фотку
    /// </summary>
    Ready,
    /// <summary>
    /// Занята: или инициализируется, или занята съемкой
    /// </summary>
    Busy
}