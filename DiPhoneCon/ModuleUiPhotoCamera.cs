using System.Diagnostics;
using System.Runtime.InteropServices;

namespace DiPhoneCon;

/// <summary>
/// Отвечает за UI Всех типов камер
/// </summary>
public class ModuleUiPhotoCamera : IUiModule
{
    private List<ModuleMenuCommand> menuCommands = new List<ModuleMenuCommand>();

    public ModuleUiPhotoCamera()
    {
        menuCommands.Add(new ModuleMenuCommand(1,"Проверить состояние камеры"));
        menuCommands.Add(new ModuleMenuCommand(2,"Есть ли в памяти камеры фотка?"));
        menuCommands.Add(new ModuleMenuCommand(3,"Сфоткать"));
        menuCommands.Add(new ModuleMenuCommand(4,"Получить последнюю фотку"));
    }
    
    /// <inheritdoc />
    public UiCode RunModuleUi(BaseDevice device, IDeviceModule module)
    {
        IModulePhotoCamera camera = module as IModulePhotoCamera;
        
        UserInterface ui = new UserInterface();
        var result = ui.ShowMenu(menuCommands.ToArray(), "Что будем делать?", "Выключить камеру", x => x.Title);

        if (result.Code != UiCode.Variant) return result.Code;

        if (result.Payload.Code == 1)
        {
            if (camera.State==PhotoCameraState.Busy) Console.WriteLine("Камера работает, подождите секундочку");
            else if (camera.State==PhotoCameraState.Ready) Console.WriteLine("Камера готова, пользуйтесь");
            return result.Code;
        }
        else if (result.Payload.Code == 2)
        {
            if (camera.HasLastPhoto) Console.WriteLine("В камере есть фотка");
            else Console.WriteLine("В камере фотки нет");
            return result.Code;
        }
        else if (result.Payload.Code == 3)
        {
            var photo = camera.MakePhoto();
            if (!string.IsNullOrWhiteSpace(photo?.ImageUrl))
            {
                OpenBrowser(photo.ImageUrl);
            }
            return result.Code;
        }
        else if (result.Payload.Code == 4)
        {
            if (camera.HasLastPhoto)
            {
                var photo = camera.GetLastPhoto();
                if (!string.IsNullOrWhiteSpace(photo?.ImageUrl))
                {
                    OpenBrowser(photo.ImageUrl);
                }
            }
            else
            {
                Console.WriteLine("Фоток в камере нет, сначала щелкните чего-нибудь");
            }

            return result.Code;
        }

        return UiCode.Unknown;
    }
    
    
    public static void OpenBrowser(string url)
    {
        try
        {
            Process.Start(url);
        }
        catch
        {
            // hack because of this: https://github.com/dotnet/corefx/issues/10361
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                url = url.Replace("&", "^&");
                Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                Process.Start("xdg-open", url);
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                Process.Start("open", url);
            }
            else
            {
                throw;
            }
        }
    }
}