﻿namespace DiPhoneCon;

/// <summary>
/// Интерфейс взаимодействия с модулем устройства
/// </summary>
public interface IUiModule
{
    UiCode RunModuleUi(BaseDevice device, IDeviceModule module);
}