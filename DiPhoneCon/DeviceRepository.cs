﻿namespace DiPhoneCon;

/// <summary>
/// репозиторий доступных для ощупывания устройств
/// </summary>
public class DeviceRepository
{
    public List<BaseDevice> devices = new List<BaseDevice>();

    public DeviceRepository()
    {
        //инициализируем репозиторий хардкодно - т.к. задание искусственное
        devices.Add(new DeviceWiredPhone());
        devices.Add(new DeviceWirelessPhone());
        devices.Add(new WirelessCameraPhone());
        devices.Add(new HandCam());
        devices.Add(new PocketNote());
    }

    /// <summary>
    /// Все доступные для использования устройства
    /// </summary>
    /// <returns></returns>
    public BaseDevice[] GetDevices()
    {
        return devices.ToArray();
    }
}