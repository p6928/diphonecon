using System.Runtime.CompilerServices;

namespace DiPhoneCon;

/// <summary>
/// Отвечает за UI всех звонилок
/// </summary>
public class ModuleUiVoiceCall : IUiModule
{
    private List<ModuleMenuCommand> menuCommands = new List<ModuleMenuCommand>();
    public ModuleUiVoiceCall()
    {
        menuCommands.Add(new ModuleMenuCommand(1,"Прочитать IMEI"));
        menuCommands.Add(new ModuleMenuCommand(2,"Прочитать номер своей SIM-карты"));
        menuCommands.Add(new ModuleMenuCommand(3,"Воткнуть новую симку"));
        menuCommands.Add(new ModuleMenuCommand(4,"Подключиться к базовой станции"));
        menuCommands.Add(new ModuleMenuCommand(5,"Позвонить по номеру"));
    }
    
    /// <inheritdoc />
    public UiCode RunModuleUi(BaseDevice device, IDeviceModule module)
    {
        IModuleVoiceCall caller = module as IModuleVoiceCall;

        UserInterface ui = new UserInterface();
        var result = ui.ShowMenu(menuCommands.ToArray(), "Что будем делать?", "Выйти из звонилки", x => x.Title);

        if (result.Code != UiCode.Variant) return result.Code;

        if (result.Payload.Code == 1)
        {
            //Читаем IMEI
            Console.WriteLine($"IMEI: {caller.IMEI}");
            return result.Code;
        }
        else if (result.Payload.Code == 2)
        {
            Console.WriteLine($"Ваш номер телефона: {caller.GetSim()}");
            return result.Code;
        }
        else if (result.Payload.Code == 3)
        {
            Console.WriteLine($"Вставьте вашу симку в устройство (введите ее номер): ");
            string newSimNum = Console.ReadLine();
            caller.SetSim(newSimNum);
            return result.Code;
        }
        else if (result.Payload.Code == 4)
        {
            caller.ConnectBaseStation();
            return result.Code;
        }
        else if (result.Payload.Code == 5)
        {
            Console.WriteLine("Введите номер и мы позвоним: ");
            string number = Console.ReadLine();
            caller.CallToNumber(number);
            return result.Code;
        }

        return UiCode.Unknown;
    }
}