﻿// See https://aka.ms/new-console-template for more information

using DiPhoneCon;

Console.WriteLine("Добро пожаловать на DiPhoneCon!");
Console.WriteLine("Здесь вы можете потрогать и попробовать наши новинки");

//Устройство в руках у пользователя
BaseDevice currentDevice = null;
//Выбранный модуль устройства, с которым пользователь буде взаимодействовать
IDeviceModule currentModule = null;

//репозиторий устройств
DeviceRepository _repo = new DeviceRepository();
//Интерфейс для взаимодействия с устройствами
UserInterface ui = new UserInterface();


do
{
    Console.WriteLine("");
    //Выбираем устройство
    if (currentDevice is null)
    {
        Console.WriteLine("Какое устройство вы хотите взять в руки?");
        var selectDeviceResult = ui.ShowMenu(_repo.GetDevices(), "Какое устройство вы хотите взять в руки?",
            null, x => x.Title);
        if (selectDeviceResult.Code == UiCode.Exit) return;
        else if (selectDeviceResult.Code == UiCode.Unknown) continue;
        currentDevice = selectDeviceResult.Payload;
    }
    else Console.WriteLine($"У вас в руках '{currentDevice.Title}'");
    
    //Выбираем модуль
    if (currentModule is null)
    {
        Console.WriteLine($"Какой модуль использовать?");
        var selectModuleResult = ui.ShowMenu(currentDevice.Modules, "В какой модуль вы хотите войти?",
            "Положить устройство обратно", x => x.Title);
        if (selectModuleResult.Code == UiCode.Exit) return;
        else if (selectModuleResult.Code == UiCode.Unknown) continue;
        else if (selectModuleResult.Code == UiCode.Back)
        {
            currentDevice = null;
            continue;
        }
        currentModule = selectModuleResult.Payload;
    }
    else Console.WriteLine($"Открыт модуль '{currentModule.Title}'");
    
    //Взаимодействуем с модулем.
    if (currentDevice is not null && currentModule is not null)
    {
        var result = ui.ModuleInteraction(currentDevice, currentModule);
        if (result == UiCode.Back) currentModule = null;
        else if (result == UiCode.Exit) return;
        else if (result == UiCode.Unknown)
        {
            Console.WriteLine("Мы не поняли, чего вы хотели.");
            currentModule = null;
        }
    }

}while(true);