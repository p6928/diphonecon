namespace DiPhoneCon;

/// <summary>
/// Адресная книга - единая для всех моделей телефонов
/// </summary>
public class AddressBook : IDeviceModule
{
    /// <summary>
    /// Коллекция записей адресной книги
    /// </summary>
    private List<AddressBookRecord> _list = new List<AddressBookRecord>();

    /// <summary>
    /// Конструктор заглушка, создающий тестовые записи в адресной книке.
    /// </summary>
    /// <remarks>
    /// В задании нет задачи реализовать полноценную работу с адресной книгой
    /// </remarks>
    public AddressBook()
    {
        testInit();
    }

    /// <summary>
    /// Получить все записи
    /// </summary>
    /// <returns></returns>
    public AddressBookRecord[] GetAllRecords()
    {
        return _list.ToArray();
    }

    //найти запись по id
    public AddressBookRecord Find(string id)
    {
        return _list.FirstOrDefault(x => x.Id == id);
    }


    void testInit()
    {
        _list.Add(new AddressBookRecord() { Id = "1", Name = "Хуан Лопес", PhoneNumber = "+7929757477" });
        _list.Add(new AddressBookRecord() { Id = "2", Name = "Педро Гонзалес", PhoneNumber = "+712345678" });
        _list.Add(new AddressBookRecord() { Id = "3", Name = "Лопес Лола Хуановна", PhoneNumber = "+798739485" });
    }

    /// <inheritdoc />
    public string Title => "Адресная книжка";
}