﻿namespace DiPhoneCon;

/// <summary>
/// Проводной телефон
/// </summary>
public class DeviceWiredPhone : BaseDevice
{
    /// <inheritdoc />
    public override string Title => "Проводной телефон с диском";

    /// <inheritdoc />
    public override IDeviceModule[] Modules => new IDeviceModule[]
    {
        new ModuleWiredVoiceCall(),
        new AddressBook()
    };
}

public class DeviceWirelessPhone : BaseDevice
{
    /// <inheritdoc />
    public override string Title => "Сотовый Бабушкофон без камеры";

    /// <inheritdoc />
    public override IDeviceModule[] Modules => new IDeviceModule[]
    {
        new ModuleWireless3GVoiceCall(),
        new AddressBook()
    };
}

public class WirelessCameraPhone : BaseDevice
{
    /// <inheritdoc />
    public override string Title => "Смартфон с одной камерой";

    /// <inheritdoc />
    public override IDeviceModule[] Modules => new IDeviceModule[]
    {
        new ModuleWireless3GVoiceCall(),
        new ModuleSimplePhotoCamera(),
        new AddressBook()
    };
}

public class HandCam : BaseDevice
{
    /// <inheritdoc />
    public override string Title => "Прототип видеокамеры (может только фотать, но зато очень быстро)";

    /// <inheritdoc />
    public override IDeviceModule[] Modules => new IDeviceModule[] { new ModuleSimplePhotoCamera() };
}

public class PocketNote : BaseDevice
{
    /// <inheritdoc />
    public override string Title => "Электронная записная книжка";

    /// <inheritdoc />
    public override IDeviceModule[] Modules => new IDeviceModule[] { new AddressBook() };
}