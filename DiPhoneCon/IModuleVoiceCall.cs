﻿namespace DiPhoneCon;

/// <summary>
/// Аппаратный модуль голосовых звонков
/// </summary>
public interface IModuleVoiceCall
{
    /// <summary>
    /// IMEI - телефона
    /// </summary>
    string IMEI { get; }

    /// <summary>
    /// Получить номер симкарты
    /// </summary>
    /// <returns></returns>
    string GetSim();
    
    /// <summary>
    /// Воткнуть в телефон сим карту с номером
    /// </summary>
    /// <returns></returns>
    void SetSim(string newsim);

    /// <summary>
    /// Подключиться к базовой станции
    /// </summary>
    /// <returns></returns>
    bool ConnectBaseStation();

    /// <summary>
    /// Позвонить по номеру телефона
    /// </summary>
    /// <param name="dstPhoneNumber"></param>
    /// <returns></returns>
    bool CallToNumber(string dstPhoneNumber);
}