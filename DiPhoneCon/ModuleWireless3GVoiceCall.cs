﻿namespace DiPhoneCon;

/// <summary>
/// Модуль голосовых звонков для 3G
/// </summary>
public class ModuleWireless3GVoiceCall : IModuleVoiceCall, IDeviceModule
{
    private string sim = Random.Shared.Next(100000,int.MaxValue).ToString();
    /// <inheritdoc />
    private readonly string imei = Random.Shared.Next(100000,int.MaxValue).ToString();

    public string IMEI => imei;

    /// <inheritdoc />
    public string GetSim()
    {
        return sim;
    }

    /// <inheritdoc />
    public void SetSim(string newsim)
    {
        if (!string.IsNullOrWhiteSpace(newsim)) sim = newsim;
        else Console.WriteLine("Симка вставлена не тем концом, переверните и воткните снова");
    }

    /// <inheritdoc />
    public bool ConnectBaseStation()
    {
        Console.Write("Устанавливаю соединение с базовой станцией ");
        for (int i = 0; i < 10; i++)
        {
            Console.Write(".");
            Thread.Sleep(150);
        }
        Console.WriteLine("");
        Console.WriteLine("Соединение установлено");
        return true;
    }

    /// <inheritdoc />
    public bool CallToNumber(string dstPhoneNumber)
    {
        Console.WriteLine($"Выполняем тоновый набор: {dstPhoneNumber}");
        Console.WriteLine("Connected");
        return true;
    }

    /// <inheritdoc />
    public string Title => "Голосовые звонки 3G";
}