﻿using System.Diagnostics;

namespace DiPhoneCon;

/// <summary>
/// Модуль голосовых звонков по проводу
/// </summary>
public class ModuleWiredVoiceCall : IModuleVoiceCall, IDeviceModule
{
    /// <inheritdoc />
    public string IMEI => "$%^#&$*%^# - У проводного телефона нет IMEI";

    /// <inheritdoc />
    public string GetSim()
    {
        Console.WriteLine("У проводного телефона нет симки");
        return "-";
    }

    /// <inheritdoc />
    public void SetSim(string newsim)
    {
        Console.WriteLine("У этого телефона нет дырки для симки, втыкать некуда");
    }

    /// <inheritdoc />
    public bool ConnectBaseStation()
    {
        Console.WriteLine("Втыкаем провод...");
        Thread.Sleep(1700);
        Console.WriteLine("Ага, нашел куда втыкать...");
        Thread.Sleep(1700);
        Console.WriteLine("Чего то не лезет...");
        Thread.Sleep(1700);
        Console.WriteLine("ой, надо другой стороной...");
        Thread.Sleep(1700);
        Console.WriteLine("Все в порядке, провод на месте");
        Thread.Sleep(1700);
        return true;
    }

    /// <inheritdoc />
    public bool CallToNumber(string dstPhoneNumber)
    {
        Console.WriteLine("Начинаем крутить диск телефона");
        foreach (var s in dstPhoneNumber)
        {
            Console.WriteLine($"{s}");
            if (int.TryParse(s.ToString(), out int num))
            {
                for (int i = 0; i < num; i++)
                {
                    Console.Write(".");
                    Thread.Sleep(200);
                }
                Console.WriteLine("");
            }
        }

        Console.WriteLine("Связь установлена, говорите.");
        return true;
    }

    /// <inheritdoc />
    public string Title => "Голосовые звонки по проводу";
}