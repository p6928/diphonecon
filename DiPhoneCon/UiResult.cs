﻿namespace DiPhoneCon;

/// <summary>
/// Результат UI взаимодействия с пользователем
/// </summary>
/// <typeparam name="T"></typeparam>
public class UiResult<T>
{
    /// <summary>
    /// Код пользовательского действия
    /// </summary>
    public UiCode Code { get; set; }
    public T Payload { get; set; }
}

/// <summary>
/// Код пользовательского ввода
/// </summary>
public enum UiCode
{
    /// <summary>
    /// Пользователь выбрал вариант из списка
    /// </summary>
    Variant,
    /// <summary>
    /// Пользователь хочет закрыть приложение
    /// </summary>
    Exit,
    /// <summary>
    /// Пользователь хочет вернуться на уровень выше (на шаг назад)
    /// </summary>
    Back,
    /// <summary>
    /// Непонятная команда
    /// </summary>
    Unknown,
}