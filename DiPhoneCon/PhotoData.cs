﻿namespace DiPhoneCon;

/// <summary>
/// объект данных с фотокамеры
/// </summary>
public class PhotoData
{
    /// <summary>
    /// Дата время, когда был сделан снимок
    /// </summary>
    public DateTime Created { get; set; }
    /// <summary>
    /// Бинарные данные фотки
    /// </summary>
    public byte[] ImageData { get; set; }

    /// <summary>
    /// Ссылка на фотку
    /// </summary>
    public string ImageUrl { get; set; }
}