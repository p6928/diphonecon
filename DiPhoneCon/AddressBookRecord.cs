﻿namespace DiPhoneCon;

/// <summary>
/// Запись адресной книги
/// </summary>
public class AddressBookRecord
{
    /// <summary>
    /// Id записи
    /// </summary>
    public string Id { get; set; }

    /// <summary>
    /// Имя абонента
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Телефонный номер абонента
    /// </summary>
    public string PhoneNumber { get; set; }
}