namespace DiPhoneCon;

/// <summary>
/// Отвечает за UI модуля адресной книжки
/// </summary>
public class ModuleUiAddressBook : IUiModule
{
    /// <inheritdoc />
    public UiCode RunModuleUi(BaseDevice device, IDeviceModule module)
    {
        AddressBook addresBook = module as AddressBook;
        
        UserInterface ui = new UserInterface();
        Console.WriteLine("Вы открыти адресную книгу, в ней есть абоненты: ");
        var users = addresBook.GetAllRecords();
        var addressBookResult = ui.ShowMenu(users, "Кому позвонить?", "Закрыть адресную книгу",
            x => $"{x.Name} ({x.PhoneNumber})");
        if (addressBookResult.Code == UiCode.Back) return UiCode.Back;
        if (addressBookResult.Code == UiCode.Exit)
        {
            return UiCode.Exit;
        }

        if (addressBookResult.Code == UiCode.Unknown) return UiCode.Unknown;
        
        //обрабатываем команду по набору номера.
            
        //найти модуль звонка.
        IModuleVoiceCall caller = device.GetModule<IModuleVoiceCall>();
        if (caller is null)
        {
            Console.WriteLine($"У '{device.Title}' нечем звонить :(");
            return UiCode.Unknown;
        }

        caller.CallToNumber(addressBookResult.Payload.PhoneNumber);
        return UiCode.Variant;
    }
}