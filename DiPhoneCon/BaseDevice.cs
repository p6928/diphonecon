﻿namespace DiPhoneCon;

/// <summary>
/// Устройство
/// </summary>
public abstract class BaseDevice
{
    /// <summary>
    /// Название устройства
    /// </summary>
    public abstract string Title { get; }

    /// <summary>
    /// Модули устройств
    /// </summary>
    public abstract IDeviceModule[] Modules { get; }

    /// <summary>
    /// Получить из устрайства модуль определенного типа
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T? GetModule<T>()
    {
        return (T)Modules.FirstOrDefault(x => x.GetType() == typeof(T) 
                                              || x.GetType().GetInterfaces().Any(z=>z==typeof(T)));
    }
}