﻿namespace DiPhoneCon;

/// <summary>
/// Аппаратный модуль камеры. низкое разрешение, без автофокусировки и зума
/// </summary>
public class ModuleSimplePhotoCamera : IModulePhotoCamera, IDeviceModule
{
    private PhotoData lastPhoto;
    private int index=0;

    private List<(string url, string thinks)> photoTargets = new List<(string url, string thinks)>();

    public ModuleSimplePhotoCamera()
    {
        photoTargets.Add(("https://j-pools.ru/assets/images/products/193/rattler.jpg","соседний павильон"));
        photoTargets.Add(("https://pro-dachnikov.com/uploads/posts/2021-12/thumbs/1638657156_150-pro-dachnikov-com-p-vid-iz-okna-moskva-foto-158.jpg","открытое окно"));
        photoTargets.Add(("https://sun9-81.userapi.com/impf/c625228/v625228417/384a8/YB7WghG0k1k.jpg?size=600x400&quality=96&sign=6d05b6d77e4b7307afbbf0c7aee83e98&type=album","продавщицу другого отдела"));
        photoTargets.Add(("https://national-travel.ru/wp-content/uploads/wtt-images/2020/03/%D0%91%D0%BE%D0%B9%20%D1%81%20%D0%B0%D0%BB%D0%BB%D0%B8%D0%B3%D0%B0%D1%82%D0%BE%D1%80%D0%BE%D0%BC.jpg","на случайного прохожего"));
        photoTargets.Add(("https://sun9-44.userapi.com/c627322/u275580908/video/y_01fbb8e1.jpg","на проезжающий автомобиль"));
    }
        
    /// <inheritdoc />
    public PhotoData MakePhoto()
    {
        Console.WriteLine($"Вы наводите камеру на {photoTargets[index].thinks}");
        Thread.Sleep(1500);
        Console.WriteLine("Слышен звук затвора...");
        Thread.Sleep(1500);
        Console.WriteLine("На экране открывается фотка...");
        Thread.Sleep(1500);
        
        lastPhoto = new PhotoData() { Created = DateTime.Now, ImageUrl = photoTargets[index].url};

        index++;
        if (index>=photoTargets.Count) index = 0;
        
        return lastPhoto;
    }

    /// <inheritdoc />
    public PhotoCameraState State => PhotoCameraState.Ready;//Stub - камера всегда готова

    /// <inheritdoc />
    public PhotoData GetLastPhoto()
    {
        return lastPhoto;
    }

    /// <inheritdoc />
    public bool HasLastPhoto => lastPhoto is not null;

    /// <inheritdoc />
    public string Title => "Примитивная камера";
}