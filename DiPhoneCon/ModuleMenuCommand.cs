﻿namespace DiPhoneCon;

/// <summary>
/// Команда меню модуля
/// </summary>
public class ModuleMenuCommand
{
    public ModuleMenuCommand()
    {
    }

    public ModuleMenuCommand(int code, string title)
    {
        Code = code;
        Title = title;
    }

    /// <summary>
    /// Код команды
    /// </summary>
    public int Code { get; set; }

    /// <summary>
    /// Название команды
    /// </summary>
    public string Title { get; set; }
}