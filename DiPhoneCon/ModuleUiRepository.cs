﻿namespace DiPhoneCon;

/// <summary>
/// Репозиторий пользовательских интерфейсов для различных модулей
/// </summary>
public class ModuleUiRepository
{
    /// <summary>
    /// Коллекция типов модулей и пользовательских интерфейсов
    /// </summary>
    //если захотим разнообразие по устройствам, добавим разные UI в зависимости от устройства
    private Dictionary<Type, IUiModule> _moduleUis = new Dictionary<Type, IUiModule>();

    public ModuleUiRepository()
    {
        //инициализируем хардкодно - только для тестового задания
        _moduleUis.Add(typeof(AddressBook),new ModuleUiAddressBook());
        _moduleUis.Add(typeof(IModuleVoiceCall),new ModuleUiVoiceCall());
        _moduleUis.Add(typeof(IModulePhotoCamera),new ModuleUiPhotoCamera());
    }

    /// <summary>
    /// Получить интерфейс пользователя для определенного модуля
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public IUiModule GetModuleUi(object module)
    {
        //нужно получить интерфейс по типу или базовому типу.

        //пытаемся получить UI для типа модуля
        if (_moduleUis.TryGetValue(module.GetType(), out IUiModule ui)) return ui;
        
        //если не нашли, то пытаемся найти UI Для интерфейса модуля
        foreach (Type moduleInterface in module.GetType().GetInterfaces())
        {
            if (_moduleUis.TryGetValue(moduleInterface, out IUiModule baseUi)) return baseUi;
        }

        return null;
    }
}